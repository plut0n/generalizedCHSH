import gc
import time

import picos as pcs
import numpy as np

from multiprocess import Pool
from pathlib import Path
from scipy.linalg import expm
from scipy.optimize import minimize

from numba import jit, njit

#--- Toolbox ---#
# Pauli operators
I = np.eye(2)
Z = np.array([[1.,0.],[0.,-1.]])
X = np.array([[0.,1.],[1.,0.]])
Y = np.array([[0.,-1j],[1j,0.]])
H = (Z+X)/np.sqrt(2)
M = (Z-X)/np.sqrt(2)

# Matrix operations
K = np.kron
mm = np.matmul

# SO(2) rotation
rot= lambda theta : np.array([[np.cos(theta),-np.sin(theta)],\
        [np.sin(theta),np.cos(theta)]])

#--- Bell operator ---#
def bell_op(a,b,theta=np.pi/4):
    """ Return Bell operator, for specific angle between measurements 
    (for each partite resp.), as mentionned in eq. 8.

    Parameters
    ----------
    a : float
        Angle between observabe, Alice side.
    b : float
        Angle between observabe, Bob side.
    theta : float
        Angle in [0,pi/4] between correlator A0(B0+B1) and A1(B0-B1).

    Returns
    -------
    bell : ndarray
        Bell operator cos(theta)A0(B0+B1)+sin(theta)A1(B0-B1).
    """
    A = lambda x,a : np.cos(a)*H + (-1)**x*np.sin(a)*M
    B = lambda x,b : np.cos(b)*Z + (-1)**x*np.sin(b)*X
    bell = np.sqrt(2)*(np.cos(theta)*K(A(0,a),B(0,b)+B(1,b)) \
            + np.sin(theta)*K(A(1,a),B(0,b)-B(1,b)))
    return bell

def max_bell(bell,debug=False):
    """ Semi-definite optimisation to get the state maximally
    violating a given Bell operator.

    Parameters
    ----------
    bell : np.ndarray
        Numpy 2d array, a Bell operator
    debug : bool
        Default is False. When set to true return the picos problem, 
        otherwise return the state.

    Returns
    -------
     : 
        Return according to debug.
    """

    bell = pcs.Constant('bell',bell)
    mb = pcs.Problem()
    rho = pcs.HermitianVariable('rho',(4,4))
    mb.add_constraint(pcs.trace(rho) == 1)
    mb.add_constraint(rho >> 0)
    mb.set_objective('max',pcs.trace(rho*bell).real)
    if debug:
        return mb
    else:
        mb.solve(solver="mosek",verbose=0)
        print("Max Violation: {}".format(mb.value))
        print("State is:\n{}".format(mb.get_variable('rho')))
        return mb.get_variable('rho')

#-- Choice of maps (dephasing and rotation) --#
def fidelity_frame(x,a):
    """Fidelity on the frame for b=0,pi/2

    Parameters
    ----------
    x : array-like
        Contains the direction of dephasing and the amout of rotation around y
    a : float
        Angle between measurement of Alice

    Return
    ------
    f_up,f_down : floats
        Fidelity for b=0 and for b=pi/2
    """
    r,d = x
    g_a = lambda a : (1+np.sqrt(2))*(np.sin(a)+np.cos(a)-1)
    strength_deph = lambda a : np.array([[1,0],[0,g_a(a)]])
    maps = lambda a,r,d : rot(r-d)@strength_deph(a)@rot(d)
    f_up = 1/4*(1+np.array([1/np.sqrt(2),1/np.sqrt(2)])\
            @maps(a,r,d)@np.array([np.cos(a),np.sin(a)]))
    f_down = 1/4*(1+np.array([1/np.sqrt(2),-1/np.sqrt(2)])\
            @maps(a,r,d)@np.array([np.cos(a),-np.sin(a)]))
    return f_up,f_down

def slope_frame(x,a,theta):
    """Slope of the convex roof taken on the frame (b=0,pi/2)

    Parameters
    ----------
    x : array-like
        Contains the direction of dephasing and the amout of rotation around y
    a : float
        Angle between measurement of Alice
    theta : float
        Correlator's weight

    Return
    ------
    slope : float
        slope of the convex roof

    """
    f_up,f_down = fidelity_frame(x,a)
    slope_up = (1-f_up)/(2*np.sqrt(2)*(1-np.cos(theta)))
    slope_down = (1-f_down)/(2*np.sqrt(2)*(1-np.sin(theta)))
    slope = max([slope_up,slope_down])
    return slope

def minimize_slope_frame(a,theta,x0=None,debug=False):
    """Minimize the slope of the convex roof 
    over dephasing direction and rotation angle .

    Parameters
    ----------
    a : float
        Angle between measurement of Alice
    theta : float
        Correlator's weight

    Return
    ------
    res : scipy.optimize.optimize.OptimizeResult
    """
    x = (np.random.rand(2)-1)*np.pi if x0 is None else x0
    bnds = ((-np.pi,np.pi),)*2
    res = minimize(slope_frame, x, args=(a,theta), 
            bounds=bnds, options={'disp':True if debug else False})
    return res

def init_slope_frame(theta,it=30):
    """ Generate list of dephasing,rotation paramtere for a range of Alice's
    angle of measurement, from the slope.

    Parameters
    ----------
    theta : float
        Weight between correlator
    it : int
        Number of iteration per Alice's choice of angle

    """
    init_param = []
    for a in np.linspace(0,np.pi/2,100):
        resl = [minimize_slope_frame(a,theta) for i in range(it)]
        funl = [r.fun for r in resl]
        minl_idx = min(range(len(funl)), key=funl.__getitem__)
        init_param.append(np.concatenate(([a],resl[minl_idx].x)))
    init_param = np.array(init_param)
    np.savetxt("./data/param_maps/{}.csv".format(theta),init_param,
            delimiter=',')

def safety_minimize_slope_frame(a,theta,it=5,debug=False):
    ip = np.loadtxt("./data/param_maps/{}.csv".format(theta),delimiter=",")
    x0 = ip[min(range(len(ip[:,0])), key = lambda i: abs(ip[:,0][i]-a))] 
    res = [minimize_slope_frame(a,theta,x0=x0[1:]) for _ in range(it)]
    slope = [r.fun for r in res]
    idx = min(range(it), key=slope.__getitem__)
    x = res[idx].x
    return x

def extraction_map(a,b,theta,rho):
    """ Extraction map apply to an inut state :math:`rho_{AB}` as define in 
    eq. 9 of the paper and apply for both alice and bob.

    Parameters
    ----------
    a : float
        Angle between observable, Alice side.
    b : float
        Angle between observable, Bob side.
    rho : ndarray
        Input state to apply the extraction map on. 2x2 Herm. matrix

    Return
    ------
    _ : ndarray
        Extraction map applied to `rho` (2x2 Herm. matrix).
    """
    r,d = safety_minimize_slope_frame(a,theta)
    g_a = lambda x : (1+np.sqrt(2))*(np.sin(x)+np.cos(x)-1)
    if theta==0 or theta==np.pi/4:
        g_b = lambda x : (1+np.sqrt(2))*(np.sin(x[0])+np.cos(x[0])-1)
    else:
        f = lambda x : (np.pi*np.log(1+(x[0]*(-4*x[1]+np.pi))/(2*x[1]**2)))/ \
            (4*np.log(-1+np.pi/(2*x[1])))
        g_b = lambda x : (1+np.sqrt(2))*(np.sin(f(x))+np.cos(f(x))-1)
    G_a = np.cos(d)*H-np.sin(d)*M
    #G_a = np.cos(a)*H+np.sin(a)*M
    G_b = Z if b>=0 and b<=theta else X
    #We have (A0xA1)x(B0xB1)=A0B0+A0B1+A1B0+A1B1 (k0,k1,k2,k3 here)
    k0 = (1+g_b([b,theta]))*rho
    k1 = (1-g_b([b,theta]))*K(I,G_b)*rho*K(I,G_b)
    k2 = (1+g_b([b,theta]))*K(G_a,I)*rho*K(G_a,I)
    k3 = (1-g_b([b,theta]))*K(G_a,G_b)*rho*K(G_a,G_b)
    #Sum everything with correct coef (minimal speed up)
    k = .25*((1+g_a(a))*(k0+k1) + (1-g_a(a))*(k2+k3))
    #And rotate around sigma_y
    rot_y = expm(r*1j*Y/2)
    k = K(rot_y,I)*k*(K(rot_y,I)).conj().T
    return k

def min_fidelity(x,beta,theta=np.pi/4,target=None,return_pb=False):
    """ Compute (semi-definite optimisation) the min(Fidelity) for a given
    Bell operator score to reach.

    Parameters
    ----------
    x : list
        List of two angles, a (b) between measurements of Alice (resp. Bob).
    beta : float
        Bell operator score to reach.
    theta : float
        Angle between correlator of the Bell operator.
    target : np.ndarray
        Target state to compute the fidelity from.
        Default set to `None`, in that case in it is computed from the
        eigenvector giving the maximum eigenvalue.
    rot : bool
        Include rotation along y in the map. Default is `True`.
    return_pb : bool
        When set to `True`, return the `picos` problem.
        Default is `False` (return the min fidelity).

    Return
    ------
     : float
        min(Fidelity) if the optimization (sdp) is succesful.
        A gradient otherwise.
    """
    a,b = x
    a = np.abs(a) % (np.pi/2)
    b = np.abs(b) % (np.pi/2)
    bell = bell_op(a,b,theta)
    fid = pcs.Problem()
    #SDP parameter
    if target is None:
        bell_target = bell_op(np.pi/4,theta,theta)
        target_state = np.linalg.eig(bell_target)[1][:,list(np.linalg.eig(
            bell_target)[0]).index(max(np.linalg.eig(
                bell_target)[0]))].reshape(4,1)
        target = pcs.Constant('target',target_state*target_state.conj().T)
    #SDP variable -> state :
    rho = pcs.HermitianVariable('rho',(4,4))
    #Constraints of the SDP :
        #SDP
    fid.add_constraint(rho >> 0)
        #Trace 1 (yeah, we are doing physics buddy)
    fid.add_constraint(pcs.trace(rho) == 1)
        #Violation of at least beta of a givem bell ineq.
    fid.add_constraint(pcs.trace(bell*rho) >= beta)
    #Objective of the SDP, min fidelity to the target state
    fid.set_objective('min',pcs.trace(target*extraction_map(a,b,theta,rho).real))
    if return_pb:
        return fid
    else:
        sol = fid.solve(solver='mosek',verbose=0,primals=None)
        if sol.claimedStatus  == 'optimal':
            return round(sol.value,7) #tol set to 10-8 so we round at 10-8 here
        else:
            v = np.array([a,b])-np.array([np.pi/4,theta])
            return 1.5+np.dot(v,v)

def min_angle(beta,theta=np.pi/4,target=None,debug=False):
    """ Minimize `min_angle` other measurement angles.

    Parameters
    ----------
    beta : float
        Bell operator score to reach.
    theta : float
        Angle between correlator of the Bell operator.
    target : np.ndarray
        Target state to compute the fidelity from.
        Default set to `None`, in that case in it is computed from the
        eigenvector giving the maximum eigenvalue.
    rot : bool
        Include rotation along y in the map. Default is `True`.
    debug : bool
        Verbose level of the minimisation. Default is `False`.

    Returns
    -------
    res : scipy.optimize.optimize.OptimizeResult
        Minimization result.
    """
    np.random.seed()
    angle = np.random.rand(2)*(np.pi/2)
    bnds = ((0., np.pi/2),)*2
    res = minimize(min_fidelity, angle, args=(beta,theta,target), 
            bounds=bnds, options={'disp':True if debug else False})
    return res

def sanity_check(fid,tol):
    sf = np.sort(fid)
    return True if sf[2]-sf[0]<=tol else False

def par_min_angle(b,theta,target,rot,it,tol):
    """ Intended for parallelization of `min_angle` with sanity_check.
    Not to use in stand-alone.
    """
    fid = [min_angle(b,theta,target,rot).fun for _ in range(it)]
    mf = min(fid)
    for s in range(20):
        if mf == 0.:
            break
        if mf < tol:
            if np.round(np.log10(mf))<=-7:
                break
            tol = 10**np.round(np.log10(mf))
        if sanity_check(fid,tol):
            break
        else:
            if s>=17:
                print("For beta {} while {} mf {}".format(b, s, mf))
            fid = [mf]+[min_angle(b,theta,target,rot).fun for _ in range(it)]
            mf = min(fid)
    return mf

def bell_theta(eps=.25, theta=np.pi/4, step=24, it=150, tol=1e-3, ps=24):
    """ min(Fidelity) as a function of the value of a Bell operator.

    Parameters
    ----------
    eps : float
        L+eps L-eps -> range of violation.
    step : int
        Number of step between local bound and quantum bound to find the min(Fidelity) for.
    it : int
        Number of iterations to run min_angle for every value of the bell operator (in order to avoid error due to a far-away starting point).

    Return
    ------
    beta : array
        Bell operator value.
    fidelity :
        min(Fidelity) for above corresponding beta.
    """
    bell_target = bell_op(np.pi/4,theta,theta)
    target_state = np.linalg.eig(bell_target)[1][:,list(np.linalg.eig(
            bell_target)[0]).index(max(np.linalg.eig(
                bell_target)[0]))].reshape(4,1)
    target = pcs.new_param('target',target_state*target_state.conj().T)
    max_bound = 2*np.sqrt(2)*np.cos(theta)
    min_bound = max_bound-((theta/(np.pi/4))**2)*eps
    print("Violation range {} to {}".format(l_meps,l_peps))
    beta = np.linspace(min_bound,max_bound,step)
    with Pool(processes=ps) as pool:
        fidelity = pool.starmap(par_min_angle, [(b,theta,target,rot,it,tol) for b in beta])
    print("Done for angle {}".format(theta))
    return beta,np.array(fidelity)

def bell_theta_smart(eps=.25, theta=np.pi/4, step=24, it=150, tol=1e-3, ps=24):
    print("Searching for beta_start (and beta_t) for weight {}:".format(theta))
    print("\tGenerating list of initial maps parameter.")
    init_slope_frame(theta)
    bell_target = bell_op(np.pi/4,theta,theta)
    target_state = np.linalg.eig(bell_target)[1][:,list(np.linalg.eig(
            bell_target)[0]).index(max(np.linalg.eig(
                bell_target)[0]))].reshape(4,1)
    target = pcs.new_param('target',target_state*target_state.conj().T)
    slope = lambda f,b: (1-f)/(2*np.sqrt(2)-b)
    max_bound = 2*np.sqrt(2)*np.cos(theta)
    min_bound = max_bound-((theta/(np.pi/4))**2)*eps
    beta = np.linspace(max_bound,min_bound,step)
    print("\tBeta fixed at {}".format(max_bound))
    fid_l = min([min_angle(max_bound,theta=theta).fun for _ in range(it)])
    print("\tFidelity min {}".format(fid_l))
    slp_l = slope(fid_l,max_bound)
    fid = [[max_bound,fid_l,slp_l]]
    for b in beta[1:]:
        print("\tBeta fixed at {}".format(b))
        min_f = min([min_angle(b,theta=theta).fun for _ in range(it)])
        slp = slope(min_f,b)
        fid.append([b,min_f,slp])
        #Check that the fidelity is lower for a lower violation
        c = 0
        while (fid[-1][1]>fid[-2][1] or fid[-1][1]>.5) and c<=5:
            print("\tWarning: Found a fidelity higher than for the previous (higher) violation. Re-minimizing.")
            del fid[-1]
            min_f = min([min_angle(b,theta=theta).fun for _ in range(it)])
            slp = slope(min_f,b)
            fid.append([b,min_f,slp])
            c += 1
        #Check if the slope is lower -> if so convex roof might be encouter
        if fid[-1][-1]<fid[-2][-1]:
            print("\tFound a lower slope, verifying...")
            if min_f>=fid[-1][1]:
                print("\tLower slope obtained, optimising again for optimal beta_star/min_f")
                min_f = min([min_angle(fid[-2][0],theta=theta).fun for _ in range(it)])
                min_f = min_f if min_f<=fid[-2][1] else fid[-2][1]
                beta_star = fid[-2][0]
                beta_t = beta_trivial(beta_star,min_f)
                print("Encounter convex roof: b_star {}, b_t {}".format(beta_star,beta_t))
                return [theta,beta_star,beta_t]
        if min_f<0.01:
            print("\tFidelity min {}".format(min_f))
            print("Encounter fid smaller than .01. Stopping...")
            fid_temp = np.array([f[:2] for f in fid])
            beta_t, beta_star = convexify(fid_temp)
            print("Encounter convex roof: b_star {}, b_t {}".format(beta_star,beta_t))
            break
        print("\tFidelity min {}".format(min_f))
    return fid

def bts(thetas,thread,eps,step,it,tol):
    time.sleep(thread*2)
    for i,tt in enumerate(thetas[::-1]):
        r = bell_theta_smart(eps=eps,theta=tt,step=step,it=it,tol=tol)
        print("Thread {} complete {}/{}".format(thread,i,thetas.shape))
        fl = open("./data/bt_500_part{}.csv".format(thread),"a+")
        fl.write(str(r)[1:-1]+"\n")
        fl.close()
        gc.collect()

def parallelized_bell_theta_smart(thetas, eps=.3, step=24, it=250, tol=1e-3, ps=20):
    thetas = thetas.reshape(int(ps),int(thetas.shape[0]/ps))
    with Pool(processes=ps) as pool:
        res = pool.starmap(bts, [(t,th,eps,step,it,tol) for th,t in enumerate(thetas)])
            

def bell_corr(angles=[0,np.pi/4],eps=None,scale="lin",step=50):
    if scale=="exp":
        angles = np.exp([angles[0]+1e-5,angles[1]])
    elif scale=="10":
        angles = [10**a for a in angles]
    theta = np.linspace(angles[0],angles[1],step+1)[1:]
    if scale=="exp":
        theta = np.log(theta)
    elif scale=="10":
        theta = np.log10(theta)
    print("Exploring angles:\n",theta)
    fidelity = np.array([[t,bell_t(eps,t)] for t in theta])
    return fidelity

def beta_trivial(beta_star,fid_star):
    slope = lambda f,b: (1-f)/(2*np.sqrt(2)-b)
    alpha = slope(fid_star, beta_star)
    beta_t = (.5-fid_star)/alpha+beta_star
    return beta_t

def convexify(bf):
    """ Find the trivial violation.
    """
    aff = lambda x: (1-x[1])/(2*np.sqrt(2)-x[0])
    x = aff([bf[:,0],bf[:,1]])
    index_max = max(range(len(x)), key=x.__getitem__)
    beta_star = bf[:,0][index_max]
    fid_star = bf[:,1][index_max]
    alpha = max(x)
    beta_t = (.5-fid_star)/alpha+beta_star
    print(beta_t,beta_star,index_max)
    return [beta_t,beta_star]

def save_mathematica_fmt(data,filename):
    """Save for mathematica post-processing

    """
    m_fmt = np.array([data.flatten()])
    np.savetxt('./data/'+filename+'_'+str(data.shape[0])+'.csv',m_fmt,
            delimiter=',')
    
def best_ineq(betat,res=0.1,savedata=False):
    """
    Parameters
    ----------
    betat : ndarray
        2d array with element [angle,corresponding beta_trivial]
    res : float
        Distance between two X/Y. Default .1
    savedata : bool
        Option to save relevant data for Mathematica exploitation.

    Returns
    -------
    ftxy : ndarray
        Format is X,Y,best angle, corresponding violation, diff with chsh
    """
    ftxy = []
    for x in np.arange(0,2*np.sqrt(2),res):
        for y in np.arange(0,2*np.sqrt(2),res):
            if x**2+y**2<=4 and x+y>=2 and x>y:
                ft = [] 
                for t in betat:
                    beta = np.sqrt(2)*(np.cos(t[0])*x+np.sin(t[0])*y)
                    if beta <= t[1]:
                        ft.append([t[0],.5])
                    else:
                        ft.append([t[0],.5+(.5/(2*np.sqrt(2)-t[1]))*(beta-t[1])])
                ind_m = np.argmax(np.array(ft)[:,1])
                ftxy.append([x,y]+ft[ind_m]+[ft[ind_m][1]-ft[-1][1]]
                        +[(ft[ind_m][1]-ft[-1][1])/ft[-1][1]])
    ftxy = np.array(ftxy)
    #Dont'l look the if below, ugly af
    #if savedata:
    #    diff_absolue = np.array([np.concatenate((f[:2],[f[-2]])) for f in ftxy])
    #    save_mathematica_fmt(diff_absolue,'diff_absolue')
    #    diff_relative = np.array([np.concatenate((f[:2],[f[-1]])) for f in ftxy])
    #    save_mathematica_fmt(diff_relative,'diff_relative') #do we care?
    #    best_angle = np.array([f[:-3] for f in ftxy if f[3]>.5])
    #    save_mathematica_fmt(best_angle,'best_angle')
    #    chsh = np.array([b for b in best_angle if b[-1]==np.pi/4])
    #    save_mathematica_fmt(chsh,'chsh')
    #    nchsh = np.array([b for b in best_angle if b[-1]!=np.pi/4])
    #    save_mathematica_fmt(nchsh,'nchsh')
    return ftxy


def find_theta(corr,betat='./betatt.csv',fidelity_prec=1e-3,theta_prec=1e-3):
    X,Y = corr
    if Path(betat).is_file():
        betat = np.loadtxt(betat,delimiter=',')
    else: 
        betat = convexify(bell_corr)

def gen_data():
    fcten = bell_corr(eps=.1,scale='10',step=30)
    ccten = np.array(convexify(fcten))
    np.savetxt('betat_log_angle.csv',ccten,delimiter=',')
    print("Log theta done.")
    fclin = bell_corr(eps=.1,scale='lin',step=30)
    cclin = np.array(convexify(fclin))
    np.savetxt('betat_lin_angle.csv',cclin,delimiter=',')
    print("Lin theta done.")
